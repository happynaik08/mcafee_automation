/**
 * 
 */
package com.mcafee.utility;

import java.io.File;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;

import com.mcafee.pages.Constant_variables;

import testingTestNG.CommunityPageTest;
import testingTestNG.HomePageTest;


/**
 * @author veenashr
 *
 */
public class ErrorScreenshot {
	
	static WebDriver driver;
	public static WebDriver getDriver(){
		if(driver==null){
			
			driver = new ChromeDriver();
		}
		return driver;
	}
	
	
	public static String captureScreenshot(WebDriver driver,String screenshotname) throws Exception
	{
		String Screenshotsfolder = null;
			TakesScreenshot ts=(TakesScreenshot)driver;
			File source=ts.getScreenshotAs(OutputType.FILE);
			
			String env_name=Constant_variables.env.toString();
			if(env_name.equalsIgnoreCase("mcafee-prod"))
			{
			Screenshotsfolder="Screenshots-prod";
			
			}
			else if (env_name.equalsIgnoreCase("mcafee-dr"))
			{
				Screenshotsfolder="Screenshots-dr";
			}
			String dest=System.getProperty("user.dir")+"\\"+Screenshotsfolder+"\\"+screenshotname+".png";
			File destination=new File(dest);
			FileUtils.copyFile(source,destination);
			return dest;
			
			
			
		
}
}
