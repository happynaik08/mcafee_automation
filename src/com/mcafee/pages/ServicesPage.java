/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

/**
 * @author veenashr
 *
 */
public class ServicesPage {
	
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader();
	
	
	public ServicesPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getServices()
	{
		return driver.findElement(By.xpath(config.getServices()));
	}
	
	public WebElement getServicesElement()
	{
		return driver.findElement(By.xpath(config.getServicesElement()));
	}
	
	public WebElement getFoundstoneServices()
	{
		return driver.findElement(By.xpath(config.getFoundstoneServices()));
	}
	
	public WebElement getFoundstoneElement()
	{
		return driver.findElement(By.xpath(config.getFoundstoneElement()));
	}
	
	public WebElement getSolutionServices()
	{
		return driver.findElement(By.xpath(config.getSolutionServices()));
	}
	
	public WebElement getSolutionServicesElement()
	{
		return driver.findElement(By.xpath(config.getSolutionServiceElement()));
	}
	
	public WebElement getEducationServices()
	{
		return driver.findElement(By.xpath(config.getEducationServices()));
	}
	
	public WebElement getEducationElement()
	{
		return driver.findElement(By.xpath(config.getEducationElement()));
	}
	

}
