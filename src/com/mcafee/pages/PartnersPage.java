/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

/**
 * @author veenashr
 *
 */
public class PartnersPage {
	
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader(); 

	
	public PartnersPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getPartners()
	{
		return driver.findElement(By.xpath(config.getPartners()));
	}
	
	public WebElement getPartenersElement()
	{
		return driver.findElement(By.xpath(config.getPartnersElement()));
	}
	
	public WebElement getPPLogin()
	{
		return driver.findElement(By.xpath(config.getPPLogin()));
	}
	
	public WebElement getPPloginelement()
	{
		return driver.findElement(By.xpath(config.getPPLoginElement()));
	}
	
	public WebElement getFindpartner()
	{
		return driver.findElement(By.xpath(config.getFindPartner()));
	}
	
	public WebElement getFindpartnerElement()
	{
		return driver.findElement(By.xpath(config.getFindPartnerElement()));
	}
	
	public WebElement getSubmit()
	{
		return driver.findElement(By.id(config.getSubmit()));
	}
	
	public WebElement getNextPage()
	{
		return driver.findElement(By.xpath(config.getNextPagePartner()));
	}
	
	public WebElement getPrevPage()
	{
		return driver.findElement(By.xpath(config.getPrevPagePartner()));
	}
	
	public WebElement getLastPage()
	{
		return driver.findElement(By.xpath(config.getLastPagePartner()));
	}
	
	public WebElement getFirstPage()
	{
		return driver.findElement(By.xpath(config.getFirstPagePartner()));
	}
	
	public WebElement getVerficationElement()
	{
		return driver.findElement(By.xpath(config.getVerificationElement()));
	}
	
	public WebElement getPPInsight()
	{
		return driver.findElement(By.xpath(config.getPPInsight()));
	}
	
	public WebElement getPPInsightElement()
	{
		return driver.findElement(By.xpath(config.getPPInsightElement()));
	}
	
	public WebElement getReseller()
	{
		return driver.findElement(By.xpath(config.getReseller()));
	}
	
	public WebElement getBecomepartner()
	{
		return driver.findElement(By.xpath(config.getBecomePartner()));
	}
	
	public WebElement getBecomepartnerElement1()
	{
		return driver.findElement(By.xpath(config.getBecomePartnerElement1()));
	}
	
	public WebElement getBecomepartnerElement2()
	{
		return driver.findElement(By.xpath(config.getBecomePartnerElement2()));
	}
	
	

}
