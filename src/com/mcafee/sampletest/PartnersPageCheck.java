/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mcafee.pages.PartnersPage;

/**
 * @author veenashr
 *
 */
public class PartnersPageCheck {
	
private WebDriver driver;


	
	public PartnersPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void ClickonPartners()
	{
		PartnersPage element=new PartnersPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement partners;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		partners= wait.until(ExpectedConditions.visibilityOf(element.getPartners()));
		partners.click();
		
		
		driver.switchTo().window(winHandleBefore);
	}
	
	public void VerifypartnersPage()
	{
		PartnersPage element=new PartnersPage(driver);
		
		
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if(element.getPartenersElement()!=null)
		{
			System.out.println("Partners Page loaded successfully");
		}
		else 
			System.out.println("Partners Page not loaded ");
		
		
	}
	
	
	public void PartnerLoginCheck()
	{
		PartnersPage element=new PartnersPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement partnerlogin;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		partnerlogin= wait.until(ExpectedConditions.visibilityOf(element.getPPLogin()));
		
		partnerlogin.click();
		//Switch focus to new window 
		String winHandleBefore2 = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if(element.getPPloginelement()!=null)
		{
			System.out.println("Partners login Page loaded successfully");
		}
		else 
			System.out.println("Partners login Page not loaded ");
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		driver.switchTo().window(winHandleBefore);
		
	}
	
	public void InsightPartnerCheck()
	{
		PartnersPage element=new PartnersPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement Insightpartner;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Insightpartner= wait.until(ExpectedConditions.visibilityOf(element.getPPInsight()));
		
		Insightpartner.click();
		
		String winHandleBefore2 = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		if(element.getPPInsightElement()!=null)
		{
			System.out.println("Partners Insight Page loaded successfully");
		}
		else 
			System.out.println("Partners Insight Page not loaded ");
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		driver.switchTo().window(winHandleBefore);
		
	}
	
	public void FindPartnerCheck() throws InterruptedException
	{
		PartnersPage element=new PartnersPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement findpartner;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		findpartner= wait.until(ExpectedConditions.visibilityOf(element.getFindpartner()));
		findpartner.click();
		
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Assert.assertEquals(true, driver.getTitle().contains("Partner Locator"));
		driver.switchTo().window(winHandleBefore);
		
		
	//click on submit
		driver.switchTo().frame(0);
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,1300)", "");
		element.getSubmit().click();
		Thread.sleep(5000);
		Assert.assertEquals(true, driver.getTitle().contains("Partner Locator"));
		
		
	//	WebElement nextpage;
		Thread.sleep(10000);
		jse.executeScript("window.scrollBy(0,2000)", "");
	
		Thread.sleep(10000);
	
		element.getLastPage().click();
		Thread.sleep(10000);
		Assert.assertEquals(true, element.getVerficationElement().isDisplayed());
		
		element.getFirstPage().click();
		Thread.sleep(10000);
		Assert.assertEquals(true, element.getVerficationElement().isDisplayed());
		
		element.getNextPage().click();
		Thread.sleep(10000);
		Assert.assertEquals(true, element.getVerficationElement().isDisplayed());
		
		element.getPrevPage().click();
		Thread.sleep(10000);
		Assert.assertEquals(true, element.getVerficationElement().isDisplayed());
		
}
	
	public void BecomePartnerCheck()
	{
		PartnersPage element=new PartnersPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement reseller;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		reseller= wait.until(ExpectedConditions.visibilityOf(element.getReseller()));
		reseller.click();
		
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		element.getBecomepartner().click();
		Assert.assertEquals(true, element.getBecomepartnerElement1().isDisplayed()&&element.getBecomepartnerElement2().isDisplayed());
		driver.switchTo().window(winHandleBefore);
		
	}

}
