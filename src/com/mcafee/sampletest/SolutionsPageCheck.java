/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mcafee.pages.SolutionsPage;



/**
 * @author veenashr
 *
 */
public class SolutionsPageCheck
{

private WebDriver driver;


	
	public SolutionsPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public void ClickonSolutions()
	{
		SolutionsPage element=new SolutionsPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement solutions;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		solutions= wait.until(ExpectedConditions.visibilityOf(element.getSolutions()));
		solutions.click();
		
		
	}
	
	public void VerifySolutions()
	{
		SolutionsPage element=new SolutionsPage(driver);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if(element.getSolutionsElement()!=null)
		{
			System.out.println("Solutions page loaded successfully");
			
		}
		else{
			System.out.println("Solutions page failed to load");
		}
		
	}
		
	public void ClickandVerifyDatacenter()
	{

		SolutionsPage element=new SolutionsPage(driver);
		
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,1000)", "");
		 
		 WebDriverWait wait = new WebDriverWait(driver,60);
			WebElement datacenter; 
			
			datacenter= wait.until(ExpectedConditions.visibilityOf(element.getDataCenter()));
			datacenter.click();
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getDatacenterElement().isDisplayed());
		
		
	}
	
	public void ClickandVerifyEmbeddedSecurity()
	{
		SolutionsPage element=new SolutionsPage(driver);
		
		 WebDriverWait wait = new WebDriverWait(driver,100);
			WebElement embeddedsec; 
			String winHandleBefore = driver.getWindowHandle();
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			embeddedsec= wait.until(ExpectedConditions.visibilityOf(element.getEmbeddedSecurity()));
			embeddedsec.click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getEmbeddedSecurityElement().isDisplayed());
		driver.switchTo().window(winHandleBefore);
	}
	
	/*public void ClickandVerifyCriticalInfra()
	{
		SolutionsPage element=new SolutionsPage(driver);
		element.getCriticalInfra().click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getCriticalInfraElement().isDisplayed());
	}*/
	
	
	
	
	


}
