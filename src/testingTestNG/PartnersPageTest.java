/**
 * 
 */
package testingTestNG;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
//import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mcafee.pages.BusinessHomePage;
import com.mcafee.pages.Constant_variables;
import com.mcafee.sampletest.EnterprisePageCheck;
import com.mcafee.sampletest.PartnersPageCheck;

import com.mcafee.utility.CopyandDeleteFiles;
import com.mcafee.utility.ErrorScreenshot;
import com.mcafee.utility.SendEmail;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author veenashr
 *
 */
@Listeners(com.mcafee.utility.JyperionListener.class)
public class PartnersPageTest {
	
	private WebDriver driver;
	ExtentReports report_PartnersPageTest;
	ExtentTest test_PartnersPageTest;
	
	@BeforeClass
	 public void M1(){
	  report_PartnersPageTest = ExtentManager.Instance();
	  
	 }
	
	@BeforeMethod
	@Parameters({"suite","envname"})
	public void beforesuite(String url_param, String env_param, Method method) throws Exception{
		
		Constant_variables.env=env_param;

		System.out.println("PartnersPageTest started");
		
		System.setProperty("webdriver.chrome.driver",Constant_variables.CHROMEDRIVER_PATH);
		//to maximize window
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(url_param);

		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		EnterprisePageCheck pageload1=new EnterprisePageCheck(driver);
		
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,250)", "");
		    
		pageload1.ClickonEnterprise();
		pageload1.isEnterprisePageLoaded();
		BusinessHomePage element=new  BusinessHomePage(driver);
		element.getMenu().click();
	
		test_PartnersPageTest=report_PartnersPageTest.startTest("PartnersPagetest - " + method.getName());
		test_PartnersPageTest.assignCategory("Sanity");
		}
	
	
	@Test(priority=6,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void PartnersPagetest()
	{
		PartnersPageCheck element = new PartnersPageCheck(driver);
		element.ClickonPartners();
		element.VerifypartnersPage();
	}
	
	@Test(priority=6,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void PartnerLogintest()
	{
		PartnersPagetest();
		PartnersPageCheck element = new PartnersPageCheck(driver);
		element.PartnerLoginCheck();
	}
	
	@Test(priority=6,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void PartnerInsighttest()
	{
		PartnersPagetest();
		PartnersPageCheck element = new PartnersPageCheck(driver);
		element.InsightPartnerCheck();
	}
	
	@Test(priority=6,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void FindPartnertest() throws InterruptedException
	{
		PartnersPagetest();
		PartnersPageCheck element = new PartnersPageCheck(driver);
		element.FindPartnerCheck();
	}
	
	@Test(priority=6,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void BecomePartnertest()
	{
		PartnersPagetest();
		PartnersPageCheck element = new PartnersPageCheck(driver);
		element.BecomePartnerCheck();		
	}
	
	@AfterMethod
	  public void teardown(ITestResult result) throws Exception 
	  {
		  if(result.getStatus()==ITestResult.FAILURE)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Failed");
			  test_PartnersPageTest.log(LogStatus.FAIL, result.getMethod().getMethodName() +" Test Method Failed");
			  //ErrorScreenshot.captureScreenshot(driver, result.getName());
			  System.out.println("Screenshot captured");
		
		}
		  else if (result.getStatus() == ITestResult.SUCCESS)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Passed");
			  test_PartnersPageTest.log(LogStatus.PASS, result.getMethod().getMethodName() +" Test Method Passed");			  
		  }
		  else if (result.getStatus() == ITestResult.SKIP)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Skipped");
			  test_PartnersPageTest.log(LogStatus.SKIP, result.getMethod().getMethodName() +" Test Method Skipped");
		  }
	   driver.close();
	   report_PartnersPageTest.endTest(test_PartnersPageTest);
	   report_PartnersPageTest.flush();
	  }
	
	
	/*@AfterSuite
	public void aftersuiteHomePage() throws Exception
	{	
	   	CopyandDeleteFiles.copyReport();
		SendEmail.zip();
		SendEmail.sendemail();
		CopyandDeleteFiles.DeleteReport();
		Thread.sleep(1000);
		Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
		driver.quit();
		
	}*/
	

}
