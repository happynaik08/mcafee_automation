/**
 * 
 */
package testingTestNG;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mcafee.pages.BusinessHomePage;
import com.mcafee.pages.Constant_variables;
import com.mcafee.sampletest.EnterprisePageCheck;
import com.mcafee.sampletest.ProductsPageCheck;
import com.mcafee.utility.CopyandDeleteFiles;
import com.mcafee.utility.ErrorScreenshot;
import com.mcafee.utility.SendEmail;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author veenashr
 *
 */

public class ProductsPageTest 
{
	private WebDriver driver;
	ExtentReports report_ProductsPageTest;
	ExtentTest test_ProductsPageTest;
	
	@BeforeClass
	 public void M1(){
	  report_ProductsPageTest = ExtentManager.Instance();
	  
	 }
	
	@BeforeMethod
	@Parameters({"suite","envname"})
	public void beforesuite(String url_param, String env_param, Method method) throws Exception{
		
		Constant_variables.env=env_param;

		System.out.println("ProductsPageTest started");
		
		System.setProperty("webdriver.chrome.driver",Constant_variables.CHROMEDRIVER_PATH);
		//to maximize window
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(url_param);
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		EnterprisePageCheck pageload1=new EnterprisePageCheck(driver);
		
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,250)", "");
		    
		pageload1.ClickonEnterprise();
		pageload1.isEnterprisePageLoaded();
		BusinessHomePage element=new  BusinessHomePage(driver);
		element.getMenu().click();
	
		test_ProductsPageTest=report_ProductsPageTest.startTest("ProductsPagetest - " + method.getName());
		test_ProductsPageTest.assignCategory("Sanity");
		}
	
	@Test(priority=2,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void ProductsPagetest()
	{
		ProductsPageCheck element=new ProductsPageCheck (driver);
		element.ClickonProducts();
		element.VerifyProductsPage();
	}
	
	@Test(priority=2,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void AllProductsPagetest()
	{	
		ProductsPageCheck element=new ProductsPageCheck (driver);
		ProductsPagetest();
		element.ClickonAllProducts();
		element.DatacenterCheck();
		element.EndpointCheck();
		element.WebProtectionCheck();
	}
	
	
	
	@AfterMethod
	  public void teardown(ITestResult result) throws Exception 
	  {
		  if(result.getStatus()==ITestResult.FAILURE)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Failed");
			  test_ProductsPageTest.log(LogStatus.FAIL, result.getMethod().getMethodName() +" Test Method Failed");
			  ErrorScreenshot.captureScreenshot(driver, result.getName());
			  System.out.println("Screenshot captured");
		
		}
		  else if (result.getStatus() == ITestResult.SUCCESS)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Passed");
			  test_ProductsPageTest.log(LogStatus.PASS, result.getMethod().getMethodName() +" Test Method Passed");			  
		  }
		  else if (result.getStatus() == ITestResult.SKIP)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Skipped");
			  test_ProductsPageTest.log(LogStatus.SKIP, result.getMethod().getMethodName() +" Test Method Skipped");
		  }
	   driver.close();
	   report_ProductsPageTest.endTest(test_ProductsPageTest);
	   report_ProductsPageTest.flush();
	  }

	/*@AfterClass
	public void tear()
	{
		report_ProductsPageTest.endTest(test_ProductsPageTest);
		report_ProductsPageTest.flush();
	}*/
	
	/*@AfterSuite
	public void aftersuiteHomePage() throws Exception
	{	
	   	CopyandDeleteFiles.copyReport();
		SendEmail.zip();
		SendEmail.sendemail();
		CopyandDeleteFiles.DeleteReport();
		Thread.sleep(1000);
		Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
		driver.quit();
		
	}*/
	
	
	
}
