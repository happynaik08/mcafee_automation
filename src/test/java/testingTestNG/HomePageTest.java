package testingTestNG;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import org.testng.annotations.Parameters;

import com.mcafee.pages.BusinessHomePage;
import com.mcafee.pages.Constant_variables;
import com.mcafee.sampletest.AboutUsPageCheck;
import com.mcafee.sampletest.EnterprisePageCheck;
import com.mcafee.sampletest.HomePageCheck;

import com.mcafee.utility.CopyandDeleteFiles;
import com.mcafee.utility.ErrorScreenshot;
import com.mcafee.utility.SendEmail;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class HomePageTest {
	
	private WebDriver driver;
	ExtentReports report_HomePageTest;
	ExtentTest test_HomePageTest;
	
	@BeforeClass
	 public void M1(){
	  report_HomePageTest = ExtentManager.Instance();
	  
	 }
	@BeforeMethod
	@Parameters({"suite","envname"})
	public void beforesuite(String url_param, String env_param) throws Exception{
		
		Constant_variables.env=env_param;
		
		System.out.println("HomePageTest started");
		
		System.setProperty("webdriver.chrome.driver",Constant_variables.CHROMEDRIVER_PATH);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		
		driver.get(url_param);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		test_HomePageTest=report_HomePageTest.startTest("HomePageTest");
		test_HomePageTest.assignCategory("Sanity");
	
		}
		  
	//@Test(priority=0,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	  public void HomePagetest() throws Exception 
	  {
			HomePageCheck pageload=new HomePageCheck(driver);
			pageload.isHomePageLoaded();
	  }
	
	//@Test(priority=0,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void BusinessPagetest() throws Exception{
		    HomePagetest();
		    JavascriptExecutor jse = (JavascriptExecutor)driver;
		    jse.executeScript("window.scrollBy(0,250)", "");
			EnterprisePageCheck pageload1=new EnterprisePageCheck(driver);
			pageload1.ClickonEnterprise();
			pageload1.isEnterprisePageLoaded();
		}
		
	
	@Test(priority=0,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void AboutUsPagetest() throws Exception{
		BusinessPagetest();
		BusinessHomePage element=new  BusinessHomePage(driver);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		element.getMenu().click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,350)", "");
		    
		AboutUsPageCheck pageload2=new AboutUsPageCheck(driver);
		pageload2.ClickonAboutUs();
		pageload2.isAboutUsPageLoaded();
		
	}
	
	
	
	/*@Test(priority=3,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void PurchasePagetest() throws Exception{
		
		BusinessPagetest();
		PurchasePageCheck pageload3=new PurchasePageCheck(driver);
		pageload3.ClickonPurchase();
		pageload3.isPurchasePageLoaded();
	}*/
	
	
	
	@AfterMethod
	  public void teardown(ITestResult result) throws Exception 
	  {
		  if(result.getStatus()==ITestResult.FAILURE)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Failed");
			  test_HomePageTest.log(LogStatus.FAIL, result.getMethod().getMethodName() +" Test Method Failed");
			  ErrorScreenshot.captureScreenshot(driver, result.getName());
			  System.out.println("Screenshot captured");
		
		}
		  else if (result.getStatus() == ITestResult.SUCCESS)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Passed");
			  test_HomePageTest.log(LogStatus.PASS, result.getMethod().getMethodName() +" Test Method Passed");			  
		  }
		  else if (result.getStatus() == ITestResult.SKIP)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Skipped");
			  test_HomePageTest.log(LogStatus.SKIP, result.getMethod().getMethodName() +" Test Method Skipped");
		  }
	   driver.close();
	   report_HomePageTest.endTest(test_HomePageTest);
	   report_HomePageTest.flush();
	  }
	
	/*@AfterClass
	public void tear()
	{
		report_HomePageTest.endTest(test_HomePageTest);
		report_HomePageTest.flush();
	}*/
	
	
		/* @AfterSuite
			public void aftersuiteHomePage() throws Exception
			{	
			   	CopyandDeleteFiles.copyReport();
				SendEmail.zip();
				SendEmail.sendemail();
				CopyandDeleteFiles.DeleteReport();
				Thread.sleep(1000);
				Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
				driver.close();
				
			}
		 */
		 
}
	
	
	
  


