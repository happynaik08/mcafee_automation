/**
 * 
 */
package testingTestNG;




import java.lang.reflect.Method;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mcafee.pages.Constant_variables;
import com.mcafee.sampletest.PrivacyPageCheck;
import com.mcafee.utility.CommonMethods;
import com.mcafee.utility.CopyandDeleteFiles;
import com.mcafee.utility.ErrorScreenshot;
import com.mcafee.utility.SendEmail;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author veenashr
 *
 */


public class PrivacyPageTest 
{
	
	private WebDriver driver;
	ExtentReports report_PrivacyPageTest;
	ExtentTest test_PrivacyPageTest;
	
	@BeforeClass
	 public void M1(){
	  report_PrivacyPageTest = ExtentManager.Instance();
	  
	 }
	@BeforeMethod
	@Parameters({"suite","envname","browser"})
	public void beforesuite(String url_param, String env_param, Method method, String browser_param) throws Exception{
                 
		Constant_variables.env=env_param;

		System.out.println("PrivacyPageTest started");
		
		if(browser_param.equalsIgnoreCase("Chrome"))
		{
		System.setProperty("webdriver.chrome.driver",Constant_variables.CHROMEDRIVER_PATH);
		//to maximize window
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		}
		else if(browser_param.equalsIgnoreCase("Firefox"))
		{
			System.setProperty("webdriver.gecko.driver","C:\\Happy\\Selenium Jars\\geckodriver-v0.15.0-win64\\geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver","C:\\Users\\hnaikx\\Downloads\\geckodriver-v0.16.1-win64\\geckodriver.exe");
			
			/*ffOptions.setBinary("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"); //Location where Firefox is installed
	 
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("moz:firefoxOptions", ffOptions);
			//set more capabilities as per your requirements
	 
			 driver = new FirefoxDriver(capabilities);*/
			
			
			driver = new FirefoxDriver();
			//driver.get("http://www.google.com");
			
		}
		
		else if(browser_param.equalsIgnoreCase("IE")){
			
			System.setProperty("webdriver.ie.driver", "C:\\Happy\\Selenium Jars\\IEDriverServer_x64_3.3.0\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
	    driver.get(url_param);
        
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,1250)", "");
        
        test_PrivacyPageTest=report_PrivacyPageTest.startTest("PrivacyPageTest - " + method.getName());
  	  	test_PrivacyPageTest.assignCategory("Sanity");
	}
	
	@Test(priority=8,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void Privacytest(){             
		
		PrivacyPageCheck element=new PrivacyPageCheck(driver);
		element.ClickonPrivacy();
		element.VerifyTab2();
		element.VerifyTab1();
		element.VerifyTab3();
	}
	
	@AfterMethod
	  public void teardown(ITestResult result) throws Exception 
	  {
		  if(result.getStatus()==ITestResult.FAILURE)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Failed");
			  test_PrivacyPageTest.log(LogStatus.FAIL, result.getMethod().getMethodName() +" Test Method Failed");
			  ErrorScreenshot.captureScreenshot(driver, result.getName());
			  System.out.println("Screenshot captured");
		
		}
		  else if (result.getStatus() == ITestResult.SUCCESS)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Passed");
			  test_PrivacyPageTest.log(LogStatus.PASS, result.getMethod().getMethodName() +" Test Method Passed");			  
		  }
		  else if (result.getStatus() == ITestResult.SKIP)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Skipped");
			  test_PrivacyPageTest.log(LogStatus.SKIP, result.getMethod().getMethodName() +" Test Method Skipped");
		  }
	   driver.close();
	   report_PrivacyPageTest.endTest(test_PrivacyPageTest);
	   report_PrivacyPageTest.flush();
	  }
	

	
	
	
	
}		  

	
	
		 
		  
		  
		  
	
	


