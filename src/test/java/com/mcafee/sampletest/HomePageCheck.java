/**
 * 
 */
package com.mcafee.sampletest;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;


import com.mcafee.pages.HomePage;


/**
 * @author veenashr
 *
 */
public class HomePageCheck 
{		
	private WebDriver driver;
	
	
	public HomePageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public void isHomePageLoaded() throws Exception
	{  
		HomePage element=new HomePage(driver);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if(element.getEnterprise()!=null)
		{
			System.out.println("HomePage load succesful ");
			
			
		}
		
		else
		{
			System.out.println("HomePage not found ");// here an email with error report has to be sent
		}
	}
	
	
	
	}



