/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.mcafee.pages.PurchasePage;

/**
 * @author veenashr
 *
 */
public class PurchasePageCheck 
{
private WebDriver driver;
	
	public PurchasePageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public void ClickonPurchase()
	{
		PurchasePage element2=new PurchasePage(driver);
		element2.getPurchase().click();
		
	}
	
	
	public void isPurchasePageLoaded() throws Exception
	{  
		PurchasePage element3=new PurchasePage(driver);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if(element3.getPurchaseelement()!=null)
		{
			System.out.println("PurchasePage load succesful ");
			element3.getFindpartner().click();
		}
		else
		{
			System.out.println("PurchasePage not found ");// here an email with error report has to be sent
		}
		
		
	}
	
	
	

}
