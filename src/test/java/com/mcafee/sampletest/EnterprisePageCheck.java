/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.mcafee.pages.BusinessHomePage;


/**
 * @author veenashr
 *
 */
public class EnterprisePageCheck 
{
	private WebDriver driver;
	
	public EnterprisePageCheck (WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void  ClickonEnterprise()
	
	{
		BusinessHomePage element1=new BusinessHomePage(driver);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		element1.getBusiness().click();
		
	}
	
	public void isEnterprisePageLoaded() throws Exception
	{  
		BusinessHomePage element1=new BusinessHomePage(driver);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		if(element1.getBusinessElement()!=null)
		{
			System.out.println("EnterprisePage load succesful ");
			
		}
		
		else
		{
			System.out.println("EnterprisePage not found ");
		}
	}
	
	
	
	
	
}
