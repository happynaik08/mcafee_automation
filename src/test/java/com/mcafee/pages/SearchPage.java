/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

/**
 * @author veenashr
 *
 */
public class SearchPage {
	
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader();
	
	
	public SearchPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getSearch()
	{
		return driver.findElement(By.xpath(config.getSearch()));
	}
	
	public WebElement getSearchTextbox()
	{
		return driver.findElement(By.xpath(config.getSearchTextbox()));
	}
	
	public WebElement getSearchResult()
	{
		return driver.findElement(By.xpath(config.getSearchResult()));
	}
	
	//navigation 
	
	public WebElement getLastPage()
	{
		return driver.findElement(By.xpath(config.getLastPageSearch()));
	}
	
	public WebElement getNextPage()
	{
		return driver.findElement(By.xpath(config.getNextPageSearch()));
	}
	
	public WebElement getFirstPage()
	{
		return driver.findElement(By.xpath(config.getFirstPageSearch()));
	}
	
	public WebElement getPrevPage()
	{
		return driver.findElement(By.xpath(config.getPrevPageSearch()));
	}
	
}
