/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;



/**
 * @author veenashr
 *
 */
public class PrivacyPage {
	
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader();
	


	
	public PrivacyPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	
	public WebElement getPrivacy()
	{	
		return driver.findElement(By.xpath(config.getPrivacy()));
	}
	
	public WebElement getTab1()
	{
		return driver.findElement(By.xpath(config.getPrivacyNotice()));
	}
	
	public WebElement getTab1Element()
	{
		return driver.findElement(By.xpath(config.getPrivacyNoticeElement()));
	}
	
	public WebElement getTab2()
	{
		return driver.findElement(By.xpath(config.getCookieNotice()));
	}
	
	public WebElement getTab2Element()
	{
		return driver.findElement(By.xpath(config.getCookieNoticeElement()));
	}
	
	public WebElement getTab3()
	{
		return driver.findElement(By.xpath(config.getMobileAppPrivacy()));
	}
	
	public WebElement getTab3Element()
	{
		return driver.findElement(By.xpath(config.getMobileAppPrivacyElement()));
	}
	

	
	
	
	
}
