/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;


/**
 * @author veenashr
 *
 */
public class HomePage 
{
	
	private WebDriver driver;
	PropertiesReader config= new PropertiesReader();

	
	
	
	
	public HomePage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getEnterprise()
	{
		return driver.findElement(By.xpath(config.getEnterprise()));
	}
	
	
		
	
	
}
