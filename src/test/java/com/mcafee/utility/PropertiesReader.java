package com.mcafee.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * @author veenashr
 *
 */
public class PropertiesReader {
	
	Properties pro;
		
		public PropertiesReader()  
		{
			 File src=new File(".//locators.properties");
			 FileInputStream fis = null;
			try {
				fis = new FileInputStream(src);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 pro=new Properties();
			  try {
				pro.load(fis);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	//About Us	   
		public String getAboutUselement()
		{
			return pro.getProperty("aboutuselement_xpath");
		}
		
		public String getAboutUs()
		{
			return pro.getProperty("aboutus_xpath");
		}
		
	//BusinessHome
		public String getBusiness()
		{
			return pro.getProperty("business_xpath");
		}
		
		public String getBusinessElement()
		{
			return pro.getProperty("businesselement_xpath");
		}
		
		public String getMenu()
		{
			return pro.getProperty("menu_xpath");
		}
		
		public String getPurchase()
		{
			return pro.getProperty("purchase_xpath");
		}
		

	//Community Page
		
		public String getCommunity()
		{
			return pro.getProperty("community_xpath");
		}
		
		public String getCommunityElement()
		{
			return pro.getProperty("communityelement_xpath");
		}
		
		public String getWebinars()
		{
			return pro.getProperty("webinars_xpath");
		}
		public String getWebinarsElement()
		{
			return pro.getProperty("webinarselement_xpath");
		}
		
		public String getEvents()
		{
			return pro.getProperty("events_xpath");
		}
		public String getEventsElement()
		{
			return pro.getProperty("eventselement_xpath");
		}
		
		public String getBlogs()
		{
			return pro.getProperty("blogs_xpath");
		}
		public String getBlogsElement()
		{
			return pro.getProperty("blogselement_xpath");
		}
	
		//HomePage
		public String getEnterprise()
		{
			return pro.getProperty("enterprise_xpath");
		}
		
		//Partners Page
		public String getPartners()
		{
			return pro.getProperty("partners_xpath");
		}
		
		public String getPartnersElement()
		{
			return pro.getProperty("partnerselement_xpath");
		}
		
		public String getPPInsight()
		{
			return pro.getProperty("PPInsight_xpath");
		}
		
		public String getPPInsightElement()
		{
			return pro.getProperty("PPInsightelement_xpath");
		}
		
		public String getFindPartner()
		{
			return pro.getProperty("findpartner_xpath");
		}
		
		public String getFindPartnerElement()
		{
			return pro.getProperty("findpartnerelement_xpath");
		}
		
		public String getSubmit()
		{
			return pro.getProperty("Submit_id");
		}
		
		public String getNextPagePartner()
		{
			return pro.getProperty("nextpagepartner_xpath");
		}
		
		public String getPrevPagePartner()
		{
			return pro.getProperty("prevpagepartner_xpath");
		}
		
		public String getLastPagePartner()
		{
			return pro.getProperty("lastpagepartner_xpath");
		}
		
		public String getFirstPagePartner()
		{
			return pro.getProperty("firstpagepartner_xpath");
		}
		
		public String getVerificationElement()
		{
			return pro.getProperty("verficationelementpartner");
		}
		
		public String getReseller()
		{
			return pro.getProperty("reseller_xpath");
		}
		
		public String getBecomePartner()
		{
			return pro.getProperty("becomepartner_xpath");
		}
		
		public String getBecomePartnerElement1()
		{
			return pro.getProperty("becomepartnerelement1_xpath");
		}
		
		public String getBecomePartnerElement2()
		{
			return pro.getProperty("becomepartnerelement2_xpath");
		}
		
		public String getPPLogin()
		{
			return pro.getProperty("PPLogin_xpath");
		}
		
		public String getPPLoginElement()
		{
			return pro.getProperty("PPloginelement_xpath");
		}
		
	//Privacy Page
		
		public String getPrivacy()
		{
			return pro.getProperty("Privacy");
		}
		
		public String getPrivacyNotice()
		{
			return pro.getProperty("Privacy_notice");
		}
		
		public String getPrivacyNoticeElement()
		{
			return pro.getProperty("Privacy_notice_element");
		}
		
		public String getCookieNotice()
		{
			return pro.getProperty("Cookie_notice");
		}
		
		public String getCookieNoticeElement()
		{
			return pro.getProperty("Cookie_notice_element");
		}
		
		public String getMobileAppPrivacy()
		{
			return pro.getProperty("Mobile_App_Privacy");
		}
		
		public String getMobileAppPrivacyElement()
		{
			return pro.getProperty("Mobile_App_Privacy_element");
		}
		
	//Products Page
		public String getProducts()
		{
			return pro.getProperty("products_xpath");
		}
		
		public String getProductsElement()
		{
			return pro.getProperty("productselement_xpath");
		}
		
		public String getAllProducts()
		{
			return pro.getProperty("allproducts_xpath");
		}
		
		public String getAllProductsElement()
		{
			return pro.getProperty("allproductselement_xpath");
		}
		
		public String getEndpointProtectionSuite()
		{
			return pro.getProperty("endpointprotectionsuite_xpath");
		}
		
		public String getWebProtection()
		{
			return pro.getProperty("webprotection_xpath");
		}
		
		public String getDataCenterSecurity()
		{
			return pro.getProperty("datacentersecurity_xpath");
		}
		
		public String getEndpointElement()
		{
			return pro.getProperty("endpointelement_xpath");
		}
		
		public String getDataCentreElement()
		{
			return pro.getProperty("datacenterelement_xpath");
		}
		
		public String getWebProtectionElement()
		{
			return pro.getProperty("webprotectionelement_xpath");
		}
		
		//SearchPage
		public String getSearch()
		{
			return pro.getProperty("search_xpath");
		}
		
		public String getSearchTextbox()
		{
			return pro.getProperty("searchtextbox_xpath");
		}
		
		public String getSearchResult()
		{
			return pro.getProperty("searchresult_xpath");
		}
		
		public String getNextPageSearch()
		{
			return pro.getProperty("nextpagesearch_xpath");
		}
		
		public String getLastPageSearch()
		{
			return pro.getProperty("lastpagesearch_xpath");
		}
		
		public String getFirstPageSearch()
		{
			return pro.getProperty("firstpagesearch_xpath");
		}
		
		public String getPrevPageSearch()
		{
			return pro.getProperty("prevpagsearch_xpath");
		}
		
		//Services Page
		public String getServices()
		{
			return pro.getProperty("services_xpath");
		}
		
		public String getServicesElement()
		{
			return pro.getProperty("serviceselement_xpath");
		}
		
		public String getFoundstoneServices()
		{
			return pro.getProperty("foundstoneservices_xpath");
		}
		
		public String getFoundstoneElement()
		{
			return pro.getProperty("foundstoneelement_xpath");
		}
		
		public String getSolutionServices()
		{
			return pro.getProperty("solutionservices_xpath");
		}
		
		public String getSolutionServiceElement()
		{
			return pro.getProperty("solutionserviceelement_xpath");
		}
		
		public String getEducationServices()
		{
			return pro.getProperty("educationservices_xpath");
		}
		
		public String getEducationElement()
		{
			return pro.getProperty("educationelement_xpath");
		}
	//Solutions Page
		
		public String getSolutions()
		{
			return pro.getProperty("Solutions_xpath");
		}
		
		public String getSolutionsElement()
		{
			return pro.getProperty("Solutionselement_xpath");
		}
		
		public String getEmbeddedSecurityElement()
		{
			return pro.getProperty("embeddedsecurityelement_xpath");
		}
		
		public String getDataCenterText()
		{
			return pro.getProperty("Datacenter_text");
		}
		
		public String getDataCenterElement()
		{
			return pro.getProperty("Datacenterelement_xpath");
		}
		
		public String getTechnology()
		{
			return pro.getProperty("technologytab_xpath");
		}
		
		public String getEmbeddedSecurity()
		{
			return pro.getProperty("embeddedsecurity_xpath");
		}
	// Support Page
		public String getSupport()
		{
			return pro.getProperty("support_xpath");
		}
		
		public String getSupportElement()
		{
			return pro.getProperty("supportelement_xpath");
		}
		
		public String getProductDwnld()
		{
			return pro.getProperty("productdwnld_xpath");
		}
		
		public String getProductDwnldBtn()
		{
			return pro.getProperty("productdwnldbtn_xpath");
		}
		
		public String getProductDwnldElement()
		{
			return pro.getProperty("productdwnldelement_xpath");
		}
		
		public String getFreeTrialBtn()
		{
			return pro.getProperty("freetrialbtn_xpath");
		}
		
		public String getProductDropdown()
		{
			return pro.getProperty("productdropdown_xpath");
		}
		
		public String getDataProtectionVerify()
		{
			return pro.getProperty("dataprotectionverify_xpath");
		}
		
		public String getEndpointProtectVerify()
		{
			return pro.getProperty("endpointprotectverify_xpath");
		}
		
		public String getWebSecurityVerify()
		{
			return pro.getProperty("websecurityverify_xpath");
		}
		
		public String getLastpageSupport()
		{
			return pro.getProperty("lastpagesupport_xpath");
		}
		
		public String getPrevpageSupport()
		{
			return pro.getProperty("prevpagesupport_xpath");
		}
		
		public String getFirstpageSupport()
		{
			return pro.getProperty("firstpagesupport_xpath");
		}
		
		public String getNextpageSupport()
		{
			return pro.getProperty("nextpagesupport_xpath");
		}
		
		public String getVerficationEleSupport()
		{
			return pro.getProperty("verificationelementsupport_xpath");
		}
		
		public String getTableElementSupport()
		{
			return pro.getProperty("tableelementsupport_xpath");
		}
		
		public String getFreetoolsBtn()
		{
			return pro.getProperty("freetoolsbtn_xpath");
		}
		
		public String getFreetoolsElement()
		{
			return pro.getProperty("freetoolselement_xpath");
		}
		
		public String getGetsUp()
		{
			return pro.getProperty("getsup_xpath");
		}
		
		public String getStinger()
		{
			return pro.getProperty("stinger_xpath");
		}
		
		public String getDownloadGetsup()
		{
			return pro.getProperty("downloadgetsup_xpath");
		}
		
		public String getDownloadStinger()
		{
			return pro.getProperty("downloadstinger_xpath");
		}
		
		public String getDownloadBtn()
		{
			return pro.getProperty("downloadbtn_xpath");
		}
		
		public String getSecurityUpdates()
		{
			return pro.getProperty("securityupdates_xpath");
		}
		
		public String getAgreebutton()
		{
			return pro.getProperty("agreebutton_xpath");
		}
		
		public String getSecurityUpdateElement()
		{
			return pro.getProperty("securityupdate_element");
		}
		
		public String getDATpackageDownload()
		{
			return pro.getProperty("datpackagedownload_xpath");
		}
	
	//ThreatCentre Page
		public String getThreatCentre()
		{
			return pro.getProperty("threatcentre_xpath");
		}
		
		public String getThreatlibrarySearchtext()
		{
			return pro.getProperty("threatlibrarysearchtext_xpath");
		}
		
		public String getThreatLibraryElement()
		{
			return pro.getProperty("threatlibraryelement_xpath");
		}
		
		public String getThreatSearchResult()
		{
			return pro.getProperty("threatsearchresult_xpath");
		}
		
		public String getDownloadcurrentDAT()
		{
			return pro.getProperty("downloadcurrentdat_xpath");
		}
		
		public String getDownloadElement()
		{
			return pro.getProperty("downloadelement_xpath");
		}
		
		public String getAgreeBtnThreat()
		{
			return pro.getProperty("agreebuttonthraet_xpath");
		}
		
		public String getNextPageThreat()
		{
			return pro.getProperty("nextpagethreat_xpath");
		}
		
		public String getLastPageThreat()
		{
			return pro.getProperty("lastpagethreat_xpath");
		}
		
		public String getFirstPageThreat()
		{
			return pro.getProperty("firstpagethreat_xpath");
		}
		
		public String getPrevPageThreat()
		{
			return pro.getProperty("prevpagethreat_xpath");
		}
		
		public String getVerifyElementThreat()
		{
			return pro.getProperty("verifyelementthreat_xpath");
		}
		
		public String getMcAfeeLabs()
		{
			return pro.getProperty("mcafeelabs_xpath");
		}
		
		public String getMcAfeeLabsReadReport()
		{
			return pro.getProperty("mcafeelabsreadreport_xpath");
		}

}
