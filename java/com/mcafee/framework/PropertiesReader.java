package com.mcafee.framework;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
*
* @author Balamurugan
*
*/

public class PropertiesReader 
{
	private static PropertiesReader instance = null;
	Properties propertiesObject;
	   private PropertiesReader() 
	   {
		   propertiesObject=new Properties();
			try 
			{
				propertiesObject.load(new InputStreamReader(new FileInputStream(".\\resources\\config.properties")));
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
	   }

	   public static PropertiesReader getInstance() 
	   {
	      if(instance == null) 
	      {
	         instance = new PropertiesReader();
	      }
	      return instance;
	   }

	   public String getData(String key)
	   {
		   return propertiesObject.getProperty(key);
	   }
}
