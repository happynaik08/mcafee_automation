package com.mcafee.framework;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
*
* @author Balamurugan
*
*/

public class BaseClass 
{
	public WebDriver driver;
	public CommonFunctions commonMethods;
	
	@BeforeClass
	public void setup()
	{
		try 
		{
			WebdriverInitializer webinit=new WebdriverInitializer(PropertiesReader.getInstance().getData("browser"));
			driver=webinit.getWebDriver();
			commonMethods=new CommonFunctions(driver);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	@AfterClass
	public void teardown()
	{
		//driver.close();
		//driver.quit();
	}
}
