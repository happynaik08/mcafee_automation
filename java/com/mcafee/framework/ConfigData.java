package com.mcafee.framework;

/**
*
* @author Balamurugan
*
*/

public class ConfigData 
{
	public static final String SFURL=PropertiesReader.getInstance().getData("sfURL");
	public static final String SFUSERNAME=PropertiesReader.getInstance().getData("sfUserName");
	public static final String SFPASSWORD=PropertiesReader.getInstance().getData("sfPassword");
	
	public static final String GMAILURL=PropertiesReader.getInstance().getData("gmailURL");
	public static final String GMAILID=PropertiesReader.getInstance().getData("gmailID");
	public static final String GMAILPASSWORD=PropertiesReader.getInstance().getData("gmailPassword");
	
	public static final String CSPURL=PropertiesReader.getInstance().getData("cspURL");
}
