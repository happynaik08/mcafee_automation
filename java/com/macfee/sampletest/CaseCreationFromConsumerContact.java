package com.macfee.sampletest;

import org.testng.annotations.BeforeClass;
import com.macfee.pages.HomePage;
import com.macfee.pages.LoginPage;
import com.mcafee.framework.BaseClass;
import com.mcafee.framework.ConfigData;

/**
*
* @author Balamurugan
*
*/

public class CaseCreationFromConsumerContact extends BaseClass
{
	private static LoginPage loginPage;
	private static HomePage home;
	
	@Override
	@BeforeClass
	public void setup() 
	{
		super.setup();
		System.out.println("Setup done");
		loginPage = commonMethods.launchSFApplication();
		home = loginPage.doLogIn(ConfigData.SFUSERNAME, ConfigData.SFPASSWORD);
	}
}
