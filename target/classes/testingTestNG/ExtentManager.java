package testingTestNG;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;

import com.relevantcodes.extentreports.ExtentReports;

/**
 * @author magesh
 *
 */
public class ExtentManager {
	    /*private static ExtentReports extent;
	    
	    public synchronized static ExtentReports getReporter(String filePath) {
	        if (extent == null) {
	        	System.out.println("extent object executed");
	            extent = new ExtentReports(filePath, true);
	            
	            extent.addSystemInfo("Environment", "QA");
	            extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent_config.xml"));
	            
	        }
	        else
	        	System.out.println("extent object already present" + extent.getProjectName());
	        
	        return extent;
	    }*/
		private static ExtentReports extent;
		public static ExtentReports Instance()
		       {
		 
		// 20150928_161823
		 Date date = new Date();  
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");  
		 String strDate= formatter.format(date);  
		 //System.out.println(strDate);  
		 //.format(Calendar.getInstance().getTime());
		 String Path =  "./test_output/Automatuion_Report.html";
		 //System.out.println(Path);
		 extent = new ExtentReports(Path, false);
		 extent.addSystemInfo("Environment", "QA");
         extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent_config.xml"));
		 
		 return extent;
		    }
		public static String CaptureScreen(WebDriver driver, String ImagesPath)
		{
		    TakesScreenshot oScn = (TakesScreenshot) driver;
		    File oScnShot = oScn.getScreenshotAs(OutputType.FILE);
		 File oDest = new File(ImagesPath+".jpg");
		 try {
		      FileUtils.copyFile(oScnShot, oDest);
		 } catch (IOException e) {System.out.println(e.getMessage());}
		 return ImagesPath+".jpg";
	        }
		
	}