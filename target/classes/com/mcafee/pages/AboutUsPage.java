/**
 * 
 */
package com.mcafee.pages;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;



/**
 * @author veenashr
 *
 */
public class AboutUsPage 
{
   private WebDriver driver;
   
   PropertiesReader config=new PropertiesReader();
  
   public AboutUsPage(WebDriver driver) 
   {
	   this.driver=driver;
   }
   


   
   public WebElement getAboutuselement(){
	   
	   return driver.findElement(By.xpath(config.getAboutUselement()));
   }
   
	public WebElement getAboutUs()
	{
		return driver.findElement(By.xpath(config.getAboutUs()));
		
	}
 
   
}
