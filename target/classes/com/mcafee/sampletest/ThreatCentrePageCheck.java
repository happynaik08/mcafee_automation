/**
 * 
 */
package com.mcafee.sampletest;



import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mcafee.pages.Constant_variables;

import com.mcafee.pages.ThreatCentrePage;
import com.mcafee.utility.CommonMethods;

/**
 * @author veenashr
 *
 */
public class ThreatCentrePageCheck {
	
private WebDriver driver;


	
	public ThreatCentrePageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public void ClickonThreatCentre()
	{
		ThreatCentrePage element=new ThreatCentrePage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement threatcentre;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		threatcentre= wait.until(ExpectedConditions.visibilityOf(element.getThreatCentre()));
		threatcentre.click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getThreatLibrarySearch().isDisplayed());
		driver.switchTo().window(winHandleBefore);
		
		
	}
// next two functions to verify threat search functionality	
	public void EnterSearchText()
	{
		ThreatCentrePage element=new ThreatCentrePage(driver);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if(element.getThreatLibrarySearch()!=null)
		{
		
		System.out.println("ThreatCentre Page loaded");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		element.getThreatLibrarySearch().clear();
		element.getThreatLibrarySearch().sendKeys("virus");
		driver.findElement(By.xpath("//*[@id='Go']")).click();
		}
		else
		{
			System.out.println("Threat Centre Page not found ");// here an email with error report has to be sent
		}
	}
	
	public void VerifySearchResults() throws Exception
	{
		ThreatCentrePage element=new ThreatCentrePage(driver);
		if(element.getThreatSearchResult()!=null)
			
		{
			System.out.println("Search results found");
			element.getThreatSearchResult().click();
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
			if(element.getDownloadCurrentDAT()!=null)
			{
				System.out.println("Search link loaded successfully");
			}
			
		}
		else 
		{
			System.out.println("Search results not found");
		}
		
		
		//to check if .dat is downloaded
		element.getDownloadCurrentDAT().click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		element.getAgreeBtn().click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		element.getDownloadElement().click();
		
		Thread.sleep(10000);
		CommonMethods element2=new CommonMethods(driver);
		element2.DownloadCheck("http://download.nai.com/products/DatFiles/4.x/NAI/avvepo8509dat.zip",Constant_variables.Downloaded_dat_path);
		File file = new File(Constant_variables.Downloaded_dat_path);
		if(file.exists())
		{
			System.out.println("File downloaded");
			file.delete();
		}
		else
		{
			System.out.println("Error downloading file");
		}
		
		
		
		
		 
		
		
}


	public void Navigationtest() throws Exception
	{
		ThreatCentrePage element=new ThreatCentrePage(driver);
		element.getLastPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true,element.getVerificationElement().isDisplayed());
		
		
		element.getPrevPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true,element.getVerificationElement().isDisplayed());
		

		element.getFirstPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true,element.getVerificationElement().isDisplayed());
	
		
		
	}
	

	

}
