package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PurchasePage
{
	private WebDriver driver;
	private By purchase_xpath=By.xpath("//*[@id='header']/div/div[2]/div[1]/ul/li[3]/a");
	private By purchaseelement_xpath=By.xpath("/html/body/div[4]/div[1]");
	
	private By findpartner_xpath=By.xpath("/html/body/div[4]/div[3]/div/div[2]/a");
	private By findpartnerelement_xpath=By.xpath("/html/body/div/div[2]/div/div/form/div[11]/div/a");	
	public PurchasePage(WebDriver driver)
	{
		this.driver=driver;  
	}
	
	public WebElement getPurchaseelement()
	{
		return driver.findElement(purchaseelement_xpath);
		
	}
	
	public WebElement getFindpartner()
	{
		return driver.findElement(findpartner_xpath);
		
	}
	
	public WebElement getFindpartnerelement()
	{
		return driver.findElement(findpartnerelement_xpath);
		
	}
	
	public WebElement getPurchase(){
		   
		   return driver.findElement(purchase_xpath);
	   }
	
}
