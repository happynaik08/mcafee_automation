/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

/**
 * @author veenashr
 *
 */ 
public class SupportPage {
	
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader();
	
	
	public SupportPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getSupport()
	{
		return driver.findElement(By.xpath(config.getSupport()));
	}
	
	public WebElement getSupportElement()
	{
		return driver.findElement(By.xpath(config.getSupportElement())	);
	}
	
	public WebElement getProductDwnld()
	{
		return driver.findElement(By.xpath(config.getProductDwnld()));
	}
	
	public WebElement getProductDwnldBtn()
	{
		return driver.findElement(By.xpath(config.getProductDwnldBtn()));
	}
	
	public WebElement getProductDwnldElement()
	{
		return driver.findElement(By.xpath(config.getProductDwnldElement()));
	}
	
	public WebElement getFreetrialBtn()
	{
		return driver.findElement(By.xpath(config.getFreeTrialBtn()));
	}
	
	public WebElement getProductDropdown()
	{
		return driver.findElement(By.xpath(config.getProductDropdown()));
	}
	
	public WebElement getDataprotectionVerify()
	{
		return driver.findElement(By.xpath(config.getDataProtectionVerify()));
	}
	
	public WebElement getEndpointSecurityVerify()
	{
		return driver.findElement(By.xpath(config.getEndpointProtectVerify()));
	}
	
	public WebElement getWebSecurityVerify()
	{
		return driver.findElement(By.xpath(config.getWebSecurityVerify()));
	}
	
	public WebElement getFreetoolsBtn()
	{
		return driver.findElement(By.xpath(config.getFreetoolsBtn()));
	}
	
	public WebElement getGetsUp()
	{
		return driver.findElement(By.xpath(config.getGetsUp()));
	}
	
	public WebElement getStinger()
	{
		return driver.findElement(By.xpath(config.getStinger()));
	}
	
	public WebElement getDownloadGetsup()
	{
		return driver.findElement(By.xpath(config.getDownloadGetsup()));
	}
	
	public WebElement getDownloadStringer()
	{
		return driver.findElement(By.xpath(config.getDownloadStinger()));
	}
	
	public WebElement getFreetoolsElement()
	{
		return driver.findElement(By.xpath(config.getFreetoolsElement()));
	}
	
	public WebElement getDownloadsBtn()
	{
		return driver.findElement(By.xpath(config.getDownloadBtn()));
	}
	

	
	public WebElement getSecurityUpdates()
	{
		return driver.findElement(By.xpath(config.getSecurityUpdates()));
	}	
	
	
	public WebElement getAgreeButton()
	{
		return driver.findElement(By.xpath(config.getAgreebutton()));
	}	
	
	public WebElement getSecurityUpdatesElement()
	{
		return driver.findElement(By.xpath(config.getSecurityUpdateElement()));
	}
	
	public WebElement getDATPackageDownload()
	{
		return driver.findElement(By.xpath(config.getDATpackageDownload()));
	}
	
	//navigation elements
	public WebElement getLastPage()
	{
		return driver.findElement(By.xpath(config.getLastpageSupport()));
	}
	
	public WebElement getPrevPage()
	{
		return driver.findElement(By.xpath(config.getPrevpageSupport()));
	}
	
	public WebElement getFirstPage()
	{
		return driver.findElement(By.xpath(config.getFirstpageSupport()));
	}
	
	public WebElement getNextPage()
	{
		return driver.findElement(By.xpath(config.getNextpageSupport()));
	}
	
	public WebElement getVerificationElement()
	{
		return driver.findElement(By.xpath(config.getVerficationEleSupport()));
	}
	
	public WebElement getTableElement()
	{
		return driver.findElement(By.xpath(config.getTableElementSupport()));
	}
	
	
	
}
