/**
 * 
 */
package com.mcafee.utility;

import java.io.File;
import java.io.IOException;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.mcafee.pages.Constant_variables;


/**
 * @author veenashr
 *
 */
public class CopyandDeleteFiles {
	
	
	static WebDriver driver;
	public static  WebDriver getDriver(){
		if(driver==null){
			
			driver = new ChromeDriver();
		}
		return driver;
	}
	
	public static void copyReport(){
		
		
		String Reportname = null;
		
		String env_name=Constant_variables.env.toString();
		String Report_path=null;
		if(env_name.equalsIgnoreCase("mcafee-prod"))
		{
		Reportname="Mcafee-prod-report";
		Report_path="Reports-prod";
		}
		else if(env_name.equalsIgnoreCase("mcafee-dr"))
		{
			Reportname="Mcafee-dr-report";
			Report_path="Reports-dr";
		}
		
		File source = new File(Constant_variables.FOLDER_PATH+Reportname+".pdf");
		File dest = new File(Constant_variables.FOLDER_PATH+"\\"+Report_path+"\\"+Reportname+".pdf");
		File temp=new File(Constant_variables.FOLDER_PATH+"\\"+Report_path+"\\"+Reportname+".pdf");
		if(temp.exists()){
			  temp.delete();
		}
		try {
		   FileUtils.copyFile(source, dest);
		
			
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		/*File source1=new File("C:\\Users\\VEENASHR\\mynewworkspace\\McAfee_Automation\\ErrorScreenshots.pdf");
		File dest1	=new File("C:\\Users\\VEENASHR\\mynewworkspace\\McAfee_Automation\\Reports\\ErrorScreenshots.pdf");
		try {
		    FileUtils.copyFile(source1, dest1);
		} catch (IOException e) {
		    e.printStackTrace();
		}
		*/
	}
	
	public static void DeleteReport()
	{	
		 
		 File file = new File(Constant_variables.REPORT_PATH_PROD);
		 file.delete();
		 File file1=new File(Constant_variables.REPORT_PATH_DR);
		 file1.delete();

	}
}
